#! /bin/awk
#
# Please note that this code is the property of the University
# of Melbourne and is Copyright 1985, 1989 by it.
# 
# All rights are reserved.
#
# Author: Jeff Schultz
#
# Extract opcodes from "bytecodes.c"
#

BEGIN {
		skip = 0;
		# Do you belive this?  It's the best I can come up with in portable
		# awk.  Incredible!
		to_lower["_"] = "_";
		to_lower["0"] = "0";
		to_lower["1"] = "1";
		to_lower["2"] = "2";
		to_lower["3"] = "3";
		to_lower["4"] = "4";
		to_lower["5"] = "5";
		to_lower["6"] = "6";
		to_lower["7"] = "7";
		to_lower["8"] = "8";
		to_lower["9"] = "9";
		to_lower["A"] = "a";
		to_lower["B"] = "b";
		to_lower["C"] = "c";
		to_lower["D"] = "d";
		to_lower["E"] = "e";
		to_lower["F"] = "f";
		to_lower["G"] = "g";
		to_lower["H"] = "h";
		to_lower["I"] = "i";
		to_lower["J"] = "j";
		to_lower["K"] = "k";
		to_lower["L"] = "l";
		to_lower["M"] = "m";
		to_lower["N"] = "n";
		to_lower["O"] = "o";
		to_lower["P"] = "p";
		to_lower["Q"] = "q";
		to_lower["R"] = "r";
		to_lower["S"] = "s";
		to_lower["T"] = "t";
		to_lower["U"] = "u";
		to_lower["V"] = "v";
		to_lower["W"] = "w";
		to_lower["X"] = "x";
		to_lower["Y"] = "y";
		to_lower["Z"] = "z";
	}

/^{"/ {
		if(skip == 1)
			next;
		split($1, a, "\"");
		op = a[2];
		printf "\t{\"%s\", c%s},\n", op, op;
		opl = op;
		printf "\t{\"";
		for(i = 1; i <= length(opl); i++)
			printf to_lower[substr(opl, i, 1)];
		printf "\", c%s},\n", opl, op;
	}

/^\#ifdef/ {
		if(skip == 1)
			exit;
		skip = 1;
		split(defines, da, " ");
		for(i in da) {
			if(("-D" $2) == da[i]) {
				skip = 0;
				next;
			}
		}
	}

/^\#endif/ {
		if(skip == 1) {
			skip = 0;
			next;
		} else
			skip = 0;
	}
