BEGIN {
		n = 0;
		skip = 0;
		# Do you belive this?  It's the best I can come up with in portable
		# awk.  Incredible!
		to_lower["_"] = "_";
		to_lower["0"] = "0";
		to_lower["1"] = "1";
		to_lower["2"] = "2";
		to_lower["3"] = "3";
		to_lower["4"] = "4";
		to_lower["5"] = "5";
		to_lower["6"] = "6";
		to_lower["7"] = "7";
		to_lower["8"] = "8";
		to_lower["9"] = "9";
		to_lower["A"] = "a";
		to_lower["B"] = "b";
		to_lower["C"] = "c";
		to_lower["D"] = "d";
		to_lower["E"] = "e";
		to_lower["F"] = "f";
		to_lower["G"] = "g";
		to_lower["H"] = "h";
		to_lower["I"] = "i";
		to_lower["J"] = "j";
		to_lower["K"] = "k";
		to_lower["L"] = "l";
		to_lower["M"] = "m";
		to_lower["N"] = "n";
		to_lower["O"] = "o";
		to_lower["P"] = "p";
		to_lower["Q"] = "q";
		to_lower["R"] = "r";
		to_lower["S"] = "s";
		to_lower["T"] = "t";
		to_lower["U"] = "u";
		to_lower["V"] = "v";
		to_lower["W"] = "w";
		to_lower["X"] = "x";
		to_lower["Y"] = "y";
		to_lower["Z"] = "z";
	}

/^{"/ {
		if(skip == 1)
			next;
		split($1, a, "\"")
		split($2, b, "}")
		bytecodes[n] = "";
		op = a[2];
		for(j = 1; j <= length(op); j++)
			bytecodes[n] = bytecodes[n] to_lower[substr(op, j, 1)];
		optypes[n] = b[1];
		n++;
		next
	}

/^\#ifdef/ {
		if(skip == 1)
			exit;
		skip = 1;
		split(defines, da, " ");
		for(i in da) {
			if(("-D" $2) == da[i]) {
				skip = 0;
				next;
			}
		}
	}

/^\#endif/ {
		if(skip == 1) {
			skip = 0;
			next;
		} else
			skip = 0;
	}

/^opcode/ {
		next;
	}

END {
		printf "\t.PRED\t'$bytecode',3\n";
		printf "%d:\tsot\t0,&%d,&%d,&%d,&%d\n", n, n+1, n+2, n+3, n+3;
		printf "%d:\tt\t3,0,&0\n", n+1;
		for(i = 1; i < n-1; i++)
			printf "\tr\t3,0,&%d\n", i;
		printf "\ttr\t3,0,&%d\n", n-1;
		printf "%d:\tsoc\t0,&(", n+2;
		for(i = 0; i < n-1; i++)
			printf "$'%s':&%d,", bytecodes[i], i;
		printf "$'%s':&%d),&%d\n", bytecodes[n-1], n-1, n+3;
		printf "%d:\tfail\n", n+3;
		for(i = 0; i < n; i++) {
			printf "\t.CLAUSE\n";
			printf "%d:\tgc\t0,$'%s'\n", i, bytecodes[i];
			printf "\tgc\t1,$%d\n", i;
			printf "\tgc\t2,$'%s'\n", optypes[i];
			printf "\tpro\n";
		}
		printf "\tlast\n";
	}
